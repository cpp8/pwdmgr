#include <iostream>
#include <string.h>
#include "impl.hpp"

using namespace std ;
int install_test(int argc, char **argv) {
    Options options("install", "install the client", "V0.0") ;

    if (options.Analyze(argc, argv)) {
        cout << "Continuing" << endl ;
    } else {
        cout << "Command Line failure" << endl ;
        exit(EXIT_FAILURE) ;
    }

    if (options.Command != INSTALL) {
        cout << "Not my function. exiting." << endl ;
        exit(EXIT_FAILURE);
    }

    Install install ;
    cout << "Install Test" << endl ;
    bool status = install.Exec(options) ;
    cout << "Install status " << status << endl ;
    return EXIT_SUCCESS ;
}

int add_test(int argc, char **argv) {
    Options options("add", "add a new password ", "V0.0") ;

    if (options.Analyze(argc, argv)) {
        cout << "Continuing" << endl ;
    } else {
        cout << "Command Line failure" << endl ;
        exit(EXIT_FAILURE) ;
    }

    if (options.Command != ADD) {
        cout << "Not my function. exiting." << endl ;
        exit(EXIT_FAILURE);
    }

    Add add ;
    cout << "ADD Test" << endl ;
    bool status = add.Exec(options) ;
    cout << "Add status " << status << endl ;
    return EXIT_SUCCESS ;
}

int main(int argc, char **argv) {
    if (argc > 1) {
        if (strcmp(argv[1],"install") == 0) {
            return install_test(argc,argv) ;
        }
        if (strcmp(argv[1],"add") == 0) {
            return add_test(argc,argv) ;
        }
    }
    return EXIT_FAILURE ;
}