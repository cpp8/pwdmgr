
#include "../options/options.hpp"

class Impl {
    public:
        virtual bool Exec(Options& _opt)  = 0;
} ;

class Install : Impl {

    public:
        bool Exec(Options& _opt) ;
} ;

class Add : Impl {

    public:
        bool Exec(Options& _opt) ;
} ;