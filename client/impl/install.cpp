#include <iostream>
#include <fstream>
#include <filesystem>
#include "impl.hpp"

using namespace std ;
namespace fs = std::filesystem;

bool Install::Exec(Options& _opt) {

    if (_opt.Username.length() < 1) {
        _opt.Username = _opt.Query("Default Username : ", _opt.Username , true );
    }

    if (_opt.Context.length() < 1) {
        _opt.Context = _opt.Query("Password server : " , _opt.Context , true );
    }

    if (fs::exists(_opt.ConfigFileName)) {
        cout << "Config file exists. Not overwriting." << endl ;
        return false ;
    }

    fs::path cfgfilename(_opt.ConfigFileName) ;
    cout << "Config file " << _opt.ConfigFileName << endl ;
    ofstream ofs (_opt.ConfigFileName) ;
    if (!ofs.is_open()) {
        cerr << "Unable to create " << _opt.ConfigFileName << endl ;
        return false ;
    }
    ofs << "context=" << _opt.Context << endl ;
    ofs << "username=" << _opt.Username << endl ;
    ofs.close() ;
    return true;    
}