#include <iostream>
#include <fstream>
#include <filesystem>
#include "impl.hpp"

using namespace std ;
namespace fs = std::filesystem;

bool Add::Exec(Options& _opt) {

    if (_opt.Username.length() < 1) {
        _opt.Username = _opt.Query("Default Username : ", _opt.Username , true );
    }

    if (_opt.Context.length() < 1) {
        _opt.Context = _opt.Query("Password server : " , _opt.Context , true );
    }

    string prompt1 = "Password for server " + _opt.Context ;
    string serverpass = _opt.Query(prompt1 , "" , false );
    string prompt2 = "Password for context " + _opt.Server + " Username " + _opt.Username ;
    string userpass = _opt.Query(prompt2 , "" , false );
    return true;    
}