#include <iostream>
#include <fstream>
#include <string>
#include "pwdmgr.pb.h"
using namespace std;

void test_user() {
    pwdmgr::User user ;
    cout << "User Initialized: " << user.IsInitialized() << endl ;
    user.set_name("srini") ;
    user.set_password("password");
    cout << "User " << user.DebugString() << endl ;
    user.SerializePartialToOstream(&cout) ;
    cout << endl ;
    cout << "User cached size " << user.GetCachedSize() << "Space Used " << user.SpaceUsedLong() << endl ;
    const google::protobuf::Descriptor *userdesc = user.descriptor() ;
    cout << "UserDesc Name " << userdesc->name() << " full name " << userdesc->full_name() << endl ;
    cout << "UserDesc DebugString " << userdesc->DebugString() << endl ;
}
int main(int argc, char **argv) {
    GOOGLE_PROTOBUF_VERIFY_VERSION;
    test_user() ;
    google::protobuf::ShutdownProtobufLibrary();

}